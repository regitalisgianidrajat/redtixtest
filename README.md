# RedTixTest

RedTix Backend Development Test

### Built With
* [Laravel](https://laravel.com/)

## Getting Started

First clone the project into your local machine

### Installation

1. Clone the project 
```sh
git clone https://gitlab.com/regitalisgianidrajat/redtixtest.git
```
2. Whenever you clone a new Laravel project you must now install all of the project dependencies. This is what actually installs Laravel itself, among other necessary packages to get started.When we run composer, it checks the composer.json file which is submitted to the github repo and lists all of the composer (PHP) packages that your repo requires. Because these packages are constantly changing, the source code is generally not submitted to github, but instead we let composer handle these updates. So to install all this source code we run composer with the following command.
```sh
composer install
```
3. Create env (env that i use is already copied at .env.example file) 
4. create database and start run the terminal then type
 ```sh
php artisan migrate
```
5. run seeder database 
```sh
php artisan db:seed
```
6. Start to view, create, update, or delete by hit the public url

### API

1. Import the collection into your postman [EventManagement](https://www.getpostman.com/collections/af1ca431ab03e40a279e)
2. Make env for postman collection - endpoint url for this api is [URL](http://localhost/redtixtest/public/api/)
```sh
//this is example env for the postman
{
	"id": "#",
	"name": "RedTix",
	"values": [
		{
			"key": "URL",
			"value": "http://localhost/redtixtest/public/api/",
			"enabled": true
		}
	]
}
```
3. to access the API you need to get credential token first by request token on postman > file API GET CREDENTIAL
```sh
//this is default auth for get the token
{
    user:AirAsiaRedTix
    secret:airasiaredtix2020
}

```
4. Set the token to the Authorization -> Barier Token

## Usage

To use this project you need to import the collection first [Collection](https://www.getpostman.com/collections/88962d3321645d92dc82)

1. API available on the collection
    1. GET ALL USER (Show user, company and event)
    2. GET DETAIL DATA (Show user, company and event based on specified id)
    3. POST USER (create user)
    4. GET COMPANY DATA
    5. GET EVENT DATA

## Note

