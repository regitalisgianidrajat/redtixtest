<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyModel extends Model
{
    use SoftDeletes;

    protected $table   = 'company';
	public $primarykey = 'company_id';
    public $timestamps = true;
    protected $fillable = [
		'company_code',
		'company_name'
	];
	protected $casts = [
		'company_code' 	=> 'string',
		'company_name' 	=> 'string'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at'
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\UserModel','company_id', 'company_id');
    }
}
