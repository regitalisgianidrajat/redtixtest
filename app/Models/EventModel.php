<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventModel extends Model
{
    use SoftDeletes;

    protected $table   = 'event';
	public $primarykey = 'event_id';
    public $timestamps = true;
    protected $fillable = [
		'event_code',
		'event_name',
		'description'
	];
	protected $casts = [
		'event_code' 	=> 'string',
		'event_name' 	=> 'string',
		'description' 	=> 'string'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at'
    ];
    public function user()
    {
        return $this->hasMany('App\Models\UserModel','event_id', 'event_id');
    }
}
