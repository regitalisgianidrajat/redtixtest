<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CompanyModel;
use Faker\Generator as Faker;

$factory->define(CompanyModel::class, function (Faker $faker) {
    return [
        'company_code' => Str::random(10),
        'company_name' => $faker->company
    ];
});
