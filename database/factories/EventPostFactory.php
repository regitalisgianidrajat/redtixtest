<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EventModel;
use Faker\Generator as Faker;

$factory->define(EventModel::class, function (Faker $faker) {
    return [
        'event_code' => Str::random(5),
        'event_name' => $faker->sentence,
        'description' => $faker->paragraph
];
});
