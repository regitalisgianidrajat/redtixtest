<?php

use Illuminate\Database\Seeder;
use App\Models\CompanyModel;
use App\Models\EventModel;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(CompanyModel::class, 5)->create();
        factory(EventModel::class, 10)->create();
        $this->call(UserTableSeeder::class);
        $this->call(CredentialTableSeeder::class);
    }
}
