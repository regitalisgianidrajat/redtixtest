<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'CMS\UserController@index');
Route::get('/user', 'CMS\UserController@index');
Route::get('/add', 'CMS\UserController@create');
Route::post('/save', 'CMS\UserController@store');
Route::get('/edit/{id}', 'CMS\UserController@show');
Route::post('/update/{id}', 'CMS\UserController@update');
Route::get('/delete/{id}', 'CMS\UserController@destroy');
//credential
Route::post('api/get-token','API\CredentialController@create');
// API
Route::group(['middleware' => ['checkToken'], 'prefix' => 'api'], function () {
    //user
    Route::get('user','API\MasterController@GetUser');
    Route::get('user/detail','API\MasterController@DetailUser');
    Route::post('user/create','API\MasterController@CreateUser');
    //company & event
    Route::get('company','API\MasterController@GetCompany');
    Route::get('event','API\MasterController@GetEvent');
});
